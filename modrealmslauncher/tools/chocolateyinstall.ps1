﻿$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url        = 'https://github.com/ModRealms/ModRealmsLauncher/releases/download/v2.5.4-release/ModRealms-Launcher-setup-2.5.4-release.exe'
$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url
  softwareName  = 'ModRealms Launcher*'
  checksum      = '7e7724a73b3e2c489a4621e18a6545743ed3d189971502b81a2fd13d2c87b038c14629e57fa8e31c3c195d724e585d5f4e61b62e4024446e2bcb94f7f2782c2e'
  checksumType  = 'sha512'
  silentArgs   = '/S /allusers'
  validExitCodes= @(0)
}
Install-ChocolateyPackage @packageArgs